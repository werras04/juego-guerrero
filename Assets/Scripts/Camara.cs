﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camara : MonoBehaviour
{
    public GameObject Target;    

    void Update()
    {
        transform.position = new Vector3(Target.transform.position.x, Target.transform.position.y, Target.transform.position.z);
    }
}
